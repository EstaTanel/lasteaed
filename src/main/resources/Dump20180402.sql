CREATE DATABASE  IF NOT EXISTS `lasteaed` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `lasteaed`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: lasteaed
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ability_to_teach`
--

DROP TABLE IF EXISTS `ability_to_teach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ability_to_teach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `since` date NOT NULL,
  `till` date NOT NULL,
  `comment` longtext,
  `subject_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  UNIQUE KEY `idthe_ability_to_teach_UNIQUE` (`id`),
  KEY `fk_ability_to_teach_person_idx` (`person_id`),
  KEY `fk_ability_to_teach_subject_idx` (`subject_id`),
  CONSTRAINT `fk_ability_to_teach_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ability_to_teach_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Siia tabelisse märgitakse kõik isikud kellel on võimekus õpetada.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ability_to_teach`
--

LOCK TABLES `ability_to_teach` WRITE;
/*!40000 ALTER TABLE `ability_to_teach` DISABLE KEYS */;
INSERT INTO `ability_to_teach` VALUES (1,'2010-01-01','2020-12-12',NULL,4,5),(2,'2011-02-15','2022-02-15',NULL,1,4),(3,'2015-03-08','2025-03-09',NULL,8,1),(4,'2013-02-08','2023-03-08',NULL,3,2),(5,'2014-12-12','2024-12-12',NULL,5,3),(6,'2009-08-23','2019-08-23',NULL,6,4),(7,'2017-02-10','2027-02-10',NULL,7,1),(8,'2018-03-03','2028-03-03',NULL,8,5),(9,'2016-07-01','2026-07-01',NULL,9,3),(10,'2009-12-12','2019-12-12',NULL,10,2),(11,'2012-04-05','2022-04-05',NULL,9,1),(12,'2014-06-23','2024-06-23',NULL,12,2),(13,'2016-03-16','2026-03-16',NULL,7,3),(14,'2008-09-01','2018-09-01',NULL,10,4),(15,'2017-10-10','2027-10-10',NULL,9,5);
/*!40000 ALTER TABLE `ability_to_teach` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `age_group`
--

DROP TABLE IF EXISTS `age_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `age_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min_age` int(11) NOT NULL,
  `max_age` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `comment` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idage_group_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Siia tabelisse märgitakse kõik laste vanusegrupid.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `age_group`
--

LOCK TABLES `age_group` WRITE;
/*!40000 ALTER TABLE `age_group` DISABLE KEYS */;
INSERT INTO `age_group` VALUES (1,4,7,'rebased',NULL),(2,3,6,'jänesed',NULL),(3,5,7,'hundid',NULL),(4,3,7,'oravad',NULL),(5,2,7,'karud',NULL);
/*!40000 ALTER TABLE `age_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(30) NOT NULL,
  `date_of_submission` date NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `application_scan` blob,
  `comment` longtext,
  `child_id` int(11) DEFAULT NULL,
  `applicant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idapplication_UNIQUE` (`id`),
  KEY `fk_application_child_idx` (`child_id`),
  KEY `fk_application_applicant_idx` (`applicant_id`),
  CONSTRAINT `fk_application_applicant` FOREIGN KEY (`applicant_id`) REFERENCES `person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_application_child` FOREIGN KEY (`child_id`) REFERENCES `person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='Siia tabelisse märgitakse kõik lasteaiaga seotud avaldused.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application`
--

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
INSERT INTO `application` VALUES (1,'63EA','2018-03-19','2018-03-20','2018-03-22','null','Koolitus on läbi jälle!',NULL,1),(2,'13TU','2017-12-01','2018-01-01','2018-01-25','null','Puhkus ja koolitus',NULL,1),(3,'10RR','2018-01-26','2018-03-02','2018-03-06','null','Koolitus',NULL,1),(4,'24HU','2016-03-01','2016-04-01',NULL,NULL,'Tööle asumine',NULL,2),(5,'26UI','2016-10-10','2016-10-30','2016-11-15',NULL,'Puhkus',NULL,2),(6,'30ES','2018-01-02','2018-02-15','2018-03-15',NULL,'Puhkus',NULL,2),(7,'31KK','2017-12-01','2018-01-26','2018-01-30',NULL,'Palgata puhkus',NULL,1),(8,'44HH','2018-01-02','2018-01-10','2018-01-12',NULL,'Koolitus',NULL,2),(9,'45EE','2017-05-03','2017-05-25','0001-01-01','null','Tööle asumine osalise koormusega',NULL,3),(10,'48TT','2017-11-15','2017-12-15','2018-01-10',NULL,'Puhkus',NULL,3),(11,'50AA','2018-02-01','2018-08-15','2018-08-31',NULL,'Puhkus',NULL,3),(12,'52ES','2018-02-15','2018-02-20','2018-02-23',NULL,'Koolitus',NULL,3),(13,'56ER','2015-01-01','2015-02-01',NULL,NULL,'Tööle asumine',NULL,4),(14,'58UU','2016-02-01','2016-03-01','2016-03-26',NULL,'Puhkus',NULL,4),(15,'62UI','2017-03-25','2017-04-16','2017-05-10',NULL,'Puhkus',NULL,4),(16,'68HT','2017-12-10','2017-12-13','2017-12-16',NULL,'Koolitus',NULL,4),(17,'69KK','2014-08-25','2014-09-01',NULL,NULL,'Tööle asumine',NULL,5),(18,'72II','2015-05-15','2015-06-01','2015-07-01',NULL,'Puhkus',NULL,5),(19,'74RE','2016-05-01','2016-05-10','2016-05-20',NULL,'Koolitus',NULL,5),(20,'75KK','2017-12-25','2018-02-01','2018-02-20',NULL,'Puhkus',NULL,5),(21,'65TT','2018-03-19','2018-03-25','0001-01-01','N','Palgata puhkus',NULL,1),(22,'70SA','2018-03-18','2018-03-21','2018-03-27','N','Koolitus',NULL,5),(54,'72UU','2018-03-20','2018-03-21','0001-01-01','N','koormuse muutmine',NULL,4);
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kindergarten`
--

DROP TABLE IF EXISTS `kindergarten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kindergarten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `register_code` varchar(20) NOT NULL,
  `name` varchar(60) NOT NULL,
  `address` varchar(100) NOT NULL,
  `comment` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Siia tabelisse märgitakse kõik lasteaiad.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kindergarten`
--

LOCK TABLES `kindergarten` WRITE;
/*!40000 ALTER TABLE `kindergarten` DISABLE KEYS */;
/*!40000 ALTER TABLE `kindergarten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personal_code` varchar(20) NOT NULL,
  `nationality` varchar(60) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `sex` char(1) NOT NULL,
  `date_of_birth` date NOT NULL,
  `postal_address` varchar(100) DEFAULT NULL,
  `mailing_address` varchar(100) DEFAULT NULL,
  `level_of_education` varchar(20) DEFAULT NULL,
  `acquired_speciality` varchar(60) DEFAULT NULL,
  `comment` longtext,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idPerson_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='Siin tabelis hoitakse kõikide inimeste andmeid, kes on mingit moodi seotud antud lasteaiaga olenemata sellest, kas nad on töötajad, lapsed, koostööpartnerid või kes iganes.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'38011024578','Eesti','Priit','Priimus','M','1980-11-02','Tallinn, Mustamäe linnaosa, Sipelga 14-2, 13423','priit','magister','alushariduse pedagoog','Tubli inimene','1111'),(2,'48109052356','Eesti','Nele','Nägus','F','1981-09-05','Tallinn, Lasnamäe linnaosa, Pallasti 28-1, 10001 ','nele.nagus@pal.ee','bakalaureus','pedagoogika','','2222'),(3,'47901295689','Eesti','Teele','Raja','F','1979-01-29','Tallinn, Kesklinna linnaosa, Tatari 12, 10116 ','teele.raja@tat.ee','bakalaureus','klassiõpetaja',NULL,'3333'),(4,'48502248965','Eesti','Vilja','Savi','F','1985-02-24','Tallinn, Põhja-Tallinna linnaosa, Sõle 10, 10612 ','vilja.savi@sol.ee','bakalaureus','noorsootöö','sageli haiguslehel','4444'),(5,'010150-113X','Soome','Matti','Matkailija','M','1950-01-01','Tallinn, Nõmme linnaosa, Möldre tee 13, 10915','matti.matkailija@mol.ee','bakalaureus','eripedagoog',NULL,'5555'),(6,'36408232759','eesti','Mart','Pääsuke','M','1960-09-13','Linda 13, Tartu','martp@gmail.com','kõrgem pedagoogiline','tants','','6666');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `name` varchar(60) NOT NULL,
  `comment` longtext,
  `age_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idsubject_UNIQUE` (`id`),
  KEY `fk_subject_age_group_idx` (`age_group_id`),
  CONSTRAINT `fk_subject_age_group` FOREIGN KEY (`age_group_id`) REFERENCES `age_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='Siia tabelisse märgitakse kõik õppeained mis on lasteaias.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (1,'peta4','peotants','',1),(2,'ju1','judo',NULL,1),(3,'keri1','keraamikaring',NULL,1),(4,'inku1','inglise keel','',1),(5,'muri1','muusikaring',NULL,2),(6,'kari1','kabering',NULL,3),(7,'jo1','jooga','',1),(8,'shta1','showtants',NULL,4),(9,'eeke1','eesti keel',NULL,5),(10,'li1','liikumine',NULL,5),(11,'luri1','lugemisring',NULL,5),(12,'ar1','arvutamine',NULL,1);
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-02  9:33:21
