package ee.bcsvaliitcompany.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Company {
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDateOfEstablishment() {
		return dateOfEstablishment;
	}
	public void setDateOfEstablishment(String dateOfEstablishment) {
		this.dateOfEstablishment = dateOfEstablishment;
	}
	public String getTradingName() {
		return tradingName;
	}
	public void setTradingName(String tradingName) {
		this.tradingName = tradingName;
	}
	public String getManagementBoard() {
		return managementBoard;
	}
	public void setManagementBoard(String managementBoard) {
		this.managementBoard = managementBoard;
	}
	public String getSupervisorBoard() {
		return supervisorBoard;
	}
	public void setSupervisorBoard(String supervisorBoard) {
		this.supervisorBoard = supervisorBoard;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public int getEmployeeCount() {
		return employeeCount;
	}
	public void setEmployeeCount(int employeeCount) {
		this.employeeCount = employeeCount;
	}
	private int id;
	private String name;
	private String dateOfEstablishment;
	private String tradingName;
	private String managementBoard;
	private String supervisorBoard;
	private String logoUrl;
	private String website;
	private String industry;
	private int employeeCount;
	
	public Company() {}
    public Company(ResultSet resultSetRow) {
    	
    	try {
    		this.id = resultSetRow.getInt(1);
    		this.name = resultSetRow.getString(2);
    		this.dateOfEstablishment = resultSetRow.getString(3);
    		this.tradingName = resultSetRow.getString(4);
    		this.managementBoard = resultSetRow.getString(5);
    		this.supervisorBoard = resultSetRow.getString(6);
    		this.logoUrl = resultSetRow.getString(7);
    		this.website = resultSetRow.getString(8);
    		this.industry = resultSetRow.getString(9);
    		this.employeeCount = resultSetRow.getInt(10);
    	} catch (SQLException e) {
    		e.printStackTrace();
    	
    		
    		
    	}
    
    }
    
}
