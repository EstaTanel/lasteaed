package ee.bcs.valiit.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import lasteaed.Ability;
import lasteaed.Application;
import lasteaed.Person;
import lasteaed.PersonService;
import lasteaed.Subject;

@Path("/")
public class RestResource {

	@GET
	@Path("/getauthenticatedperson")
	@Produces(MediaType.APPLICATION_JSON)
	public Person getAuthenticatedPerson(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return (Person) session.getAttribute("activeperson");
		}
		return new Person();
	}

	@GET
	@Path("/getlogoffperson")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLogOffPerson(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		session.removeAttribute("activeperson");
			return "OK";
	}
	

	@POST
	@Path("/getauthentication")
	public String getAuthentication(@Context HttpServletRequest req, @FormParam("mailing_address") String email,
			@FormParam("password") String pword) {
		HttpSession session = req.getSession(true);
		Person p = PersonService.getAuthentication(email, pword);
		if (p == null) {
			return "nok";
		}
		session.setAttribute("activeperson", p);
		return "ok";

	}

	@GET
	@Path("/getallagegroups")
	@Produces(MediaType.APPLICATION_JSON)
	public AgeGroup[] getAllAgeGroups(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return PersonService.getAllAgeGroups();
		}
		return null;
	}

	@GET
	@Path("/getallsubjects")
	@Produces(MediaType.APPLICATION_JSON)
	public Subject[] getAllSubjects(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return PersonService.getAllSubjects();
		}
		return null;
	}

	@GET
	@Path("/getsubject")
	@Produces(MediaType.APPLICATION_JSON)
	public Subject getSubject(@QueryParam("subject_id") String subjectId, @Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return PersonService.getSubject(subjectId);
		}
		return null;
	}

	@POST
	@Path("/savesubject")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String saveSubject(Subject subject, @Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			PersonService.saveSubject(subject);
			return "OK";
		}
		return null;
	}

	@POST
	@Path("/savepersonability")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String savePersonAbility(Ability ability, @Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			PersonService.savePersonAbility(ability);
			return "OK";
		}
		return null;
	}

	@GET
	@Path("/getpersonability")
	@Produces(MediaType.APPLICATION_JSON)
	public Ability getPersonAbility(@QueryParam("ability_id") String abilityId, @Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return PersonService.getPersonAbility(abilityId);
		}
		return null;
	}

	@GET
	@Path("/getpersonabilities")
	@Produces(MediaType.APPLICATION_JSON)
	public Ability[] getAbilities(@QueryParam("person_id") String personId, @Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return PersonService.getAbilities(personId);
		}
		return null;
	}

	@GET
	@Path("/getpersonapplication")
	@Produces(MediaType.APPLICATION_JSON)
	public Application getPersonApplication(@QueryParam("application_id") String applicationId,
			@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return PersonService.getPersonApplication(applicationId);
		}
		return null;
	}

	@POST
	@Path("/savepersonapplication")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String savePersonApplication(Application application, @Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			PersonService.savePersonApplication(application);
			return "OK";
		}
		return null;
	}

	@GET
	@Path("/getpersonapplications")
	@Produces(MediaType.APPLICATION_JSON)
	public Application[] getPersonApplications(@QueryParam("person_id") String personId,
			@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return PersonService.getPersonApplications(personId);
		}
		return null;
	}

	@POST
	@Path("/person")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String savePerson(Person person, @Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			PersonService.savePerson(person);
			return "OK";
		}
		return null;
	}

	@GET
	@Path("/getperson")
	@Produces(MediaType.APPLICATION_JSON)
	public Person getPerson(@QueryParam("person_id") String personId, @Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return PersonService.getPerson(personId);
		}
		return null;
	}

	@GET
	@Path("/persons")
	@Produces(MediaType.APPLICATION_JSON)
	public Person[] getPersons(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("activeperson") != null) {
			return PersonService.getPersons();
		}
		return null;
	}

	// @GET
	// @Path("/companies")
	// @Produces(MediaType.APPLICATION_JSON)
	// public Company[] getCompanies() {
	// return CompanyService.getCompanies();
	//
	// }
	// @POST
	// @Path("/company")
	// @Produces(MediaType.TEXT_PLAIN)
	// @Consumes(MediaType.APPLICATION_JSON)
	// public String saveCompany(Company company) {
	// CompanyService.saveCompany(company);
	//
	// return "OK";
	// }
	//
	// @GET
	// @Path("/hi/text")
	// @Produces(MediaType.TEXT_PLAIN)
	// public String getHi(@DefaultValue("World") @QueryParam("name") String name) {
	// return String.format("Hello, %s!", name);
	// }
	//
	// @GET
	// @Path("/hi/json/map/{name}")
	// @Produces(MediaType.APPLICATION_JSON)
	// public Response getHi2(@PathParam(value = "name") String name) {
	// Map<String, String> response = new HashMap<>();
	// response.put("greeting", String.format("Hello, %s!", name));
	// return Response.ok(response).build();
	// }
	//
	// @GET
	// @Path("/hi/json/dto")
	// @Produces(MediaType.APPLICATION_JSON)
	// public MessageDTO getHi3(@DefaultValue("World") @QueryParam("name") String
	// name) {
	// MessageDTO message = new MessageDTO();
	// message.setText("Hello, " + name + "!!!");
	// return message;
	// }
	//
	// @POST
	// @Path("/message")
	// @Consumes(MediaType.APPLICATION_JSON)
	// @Produces(MediaType.TEXT_PLAIN)
	// public String message(MessageDTO message) {
	// return "Sõnum oli järgmine: " + message.getText();
	// }
	//
	// @POST
	// @Path("/addnumbers")
	// @Produces(MediaType.APPLICATION_JSON)
	// public Response addNumbers(@FormParam("number1") String number1,
	// @FormParam("number2") String number2) {
	// int value1 = Integer.parseInt(number1);
	// int value2 = Integer.parseInt(number2);
	// int result = value1 + value2;
	// Map<String, String> response = new HashMap<>();
	// response.put("result", String.valueOf(result));
	// response.put("comment", "See summa arvutati kokku RestResource klassis
	// serveri pool.");
	// return Response.ok(response).build();
	// }
	//
	// @GET
	// @Path("/message/db")
	// @Produces(MediaType.TEXT_PLAIN)
	// public String getTextFromDb() throws SQLException, ClassNotFoundException {
	// // Class.forName("com.mysql.cj.jdbc.Driver");
	// Class.forName("org.mariadb.jdbc.Driver");
	// try (Connection conn =
	// DriverManager.getConnection("jdbc:mysql://localhost:3306/valiit", "root",
	// "tere")) {
	// try (Statement stmt = conn.createStatement()) {
	// try (ResultSet rs = stmt.executeQuery("SELECT simple_column FROM
	// simpledemo")) {
	// rs.first();
	// String result = rs.getString(1);
	// conn.close();
	// return result;
	// }
	// }
	// }
	// }
}
