package ee.bcs.valiit.rest;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AgeGroup {
	private int id;
	private int minAge;
	private int maxAge;
	private String name;
	private String comment;
	
	
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getMinAge() {
		return minAge;
	}



	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}



	public int getMaxAge() {
		return maxAge;
	}



	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getComment() {
		return comment;
	}



	public void setComment(String comment) {
		this.comment = comment;
	}



	public AgeGroup(ResultSet resultSetRow) {
		
	
	try {
		this.id = resultSetRow.getInt(1);
		this.minAge = resultSetRow.getInt(2);
		this.maxAge = resultSetRow.getInt(3);
		this.name = resultSetRow.getString(4);
		this.comment = resultSetRow.getString(5);
		
		

	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}
