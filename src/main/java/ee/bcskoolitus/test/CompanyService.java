package ee.bcskoolitus.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcsvaliitcompany.dao.Company;

public class CompanyService {
	private final static String CONNECTION_URL = "jdbc:mysql://localhost:3306/test2";
	private final static String CONNECTION_USERNAME = "root";
	private final static String CONNECTION_PASSWORD = "philips";
	
	public static Company[] getCompanies() {
		List<Company> companies = new ArrayList<>();
		ResultSet result = CompanyService.performSqlSelect("select * from company");
		try {
			while(result.next()) {
				companies.add(new Company(result));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return companies.toArray(new Company[0]);
		
	}
	public static void saveCompany(Company company) {
		String sql = "INSERT INTO company (name, date_of_establishment, trading_name, management_board, supervisor_board, logo_url, website, industry, employee_account)";
		sql = sql + String.format(" values ('%s','%s','%s','%s','%s','%s','%s','%s','%s')", company.getName(),
				company.getDateOfEstablishment(), company.getTradingName(), company.getManagementBoard(),
                company.getSupervisorBoard(), company.getLogoUrl(), company.getWebsite(), company.getIndustry(),
                company.getEmployeeCount());
		performSqlSelect(sql);
	
	}
	
	public static ResultSet performSqlSelect(String sql) {
		Connection conn = null;
		Statement stmt = null;
	
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			conn = DriverManager.getConnection(CONNECTION_URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
			stmt = conn.createStatement();
			return stmt.executeQuery(sql);
			
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			
		} finally {
			if (conn != null) {
				try {
					conn.close();				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			}	
		}
		return null;
		}
	}
