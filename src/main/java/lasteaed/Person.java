package lasteaed;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Person {

	private int id;
	private String personalCode;
	private String nationality;
	private String firstName;
	private String lastName;
	private String sex;
	private String dateOfBirth;
	private String postalAddress;
	private String mailingAddress;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPersonalCode() {
		return personalCode;
	}

	public void setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public String getLevelOfEducation() {
		return levelOfEducation;
	}

	public void setLevelOfEducation(String levelOfEducation) {
		this.levelOfEducation = levelOfEducation;
	}

	public String getAcquiredSpeciality() {
		return acquiredSpeciality;
	}

	public void setAcquiredSpeciality(String acquiredSpeciality) {
		this.acquiredSpeciality = acquiredSpeciality;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	private String levelOfEducation;
	private String acquiredSpeciality;
	private String comment;

	public Person() {
	}

	public Person(ResultSet resultSetRow) {

		try {
			this.id = resultSetRow.getInt(1);
			this.personalCode = resultSetRow.getString(2);
			this.nationality = resultSetRow.getString(3);
			this.firstName = resultSetRow.getString(4);
			this.lastName = resultSetRow.getString(5);
			this.sex = resultSetRow.getString(6);
			this.dateOfBirth = resultSetRow.getString(7);
			this.postalAddress = resultSetRow.getString(8);
			this.mailingAddress = resultSetRow.getString(9);
			this.levelOfEducation = resultSetRow.getString(10);
			this.acquiredSpeciality = resultSetRow.getString(11);
			this.comment = resultSetRow.getString(12);
		} catch (SQLException e) {
			e.printStackTrace();

		}

	}

}
