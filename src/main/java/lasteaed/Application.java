package lasteaed;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Application {

	private int id;
	private String number;
	private String dateOfSubmission;
	private String startDate = "000-01-01";
	private String endDate = "000-01-01";
	private String applicationScan = "N";
	private String comment;
	private int applicantId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDateOfSubmission() {
		return dateOfSubmission;
	}

	public void setDateOfSubmission(String dateOfSubmission) {
		this.dateOfSubmission = dateOfSubmission;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getApplicationScan() {
		return applicationScan;
	}

	public void setApplicationScan(String applicationScan) {
		this.applicationScan = applicationScan;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getApplicantId() {
		return applicantId;
	}

	public void setApplicantId(int applicantId) {
		this.applicantId = applicantId;
	}

	public Application() {
	}

	public Application(ResultSet resultSetRow) {

		try {
			this.id = resultSetRow.getInt(1);
			this.number = resultSetRow.getString(2);
			this.dateOfSubmission = resultSetRow.getString(3);
			this.startDate = resultSetRow.getString(4);
			this.endDate = resultSetRow.getString(5);
			this.applicationScan = resultSetRow.getString(6);
			this.comment = resultSetRow.getString(7);
			this.applicantId = resultSetRow.getInt("applicant_id");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}