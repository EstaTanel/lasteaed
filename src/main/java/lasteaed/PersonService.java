package lasteaed;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.valiit.rest.AgeGroup;

public class PersonService {
	private final static String CONNECTION_URL = "jdbc:mysql://localhost:3306/lasteaed";
	private final static String CONNECTION_USERNAME = "root";
	private final static String CONNECTION_PASSWORD = "philips";

	public static Person[] getPersons() {
		List<Person> persons = new ArrayList<>();
		ResultSet result = PersonService.performSqlSelect("select * from person");
		try {
			while (result.next()) {
				persons.add(new Person(result));

			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return persons.toArray(new Person[0]);

	}

	public static void savePerson(Person person) {
		String sql = null;
		if (person.getId() > 0) {
			sql = String.format(
					"UPDATE person SET personal_code = '%s', nationality = '%s', first_name = '%s', last_name = '%s', sex = '%s', date_of_birth = '%s', postal_address = '%s', mailing_address = '%s', level_of_education = '%s', acquired_speciality = '%s', comment = '%s' WHERE id = '%s'",
					person.getPersonalCode(), person.getNationality(), person.getFirstName(), person.getLastName(),
					person.getSex(), person.getDateOfBirth(), person.getPostalAddress(), person.getMailingAddress(),
					person.getLevelOfEducation(), person.getAcquiredSpeciality(), person.getComment(), person.getId());

		} else {
			sql = "INSERT INTO person (personal_code, nationality, first_name, last_name, sex, date_of_birth, postal_address, mailing_address, level_of_education, acquired_speciality, comment)";
			sql = sql + String.format(" values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
					person.getPersonalCode(), person.getNationality(), person.getFirstName(), person.getLastName(),
					person.getSex(), person.getDateOfBirth(), person.getPostalAddress(), person.getMailingAddress(),
					person.getLevelOfEducation(), person.getAcquiredSpeciality(), person.getComment());
		}
		performSqlSelect(sql);
	}

	public static void savePersonApplication(Application application) {
		String sql = null;
		if (application.getId() > 0) {
			sql = String.format(
					"UPDATE application SET number = '%s', date_of_submission = '%s', start_date = '%s', end_date = '%s', application_scan = '%s', comment = '%s', applicant_id = '%s' WHERE id = '%s'",
					application.getNumber(), application.getDateOfSubmission(), application.getStartDate(),
					application.getEndDate(), application.getApplicationScan(), application.getComment(),
					application.getApplicantId(), application.getId());

		} else {
			sql = "INSERT INTO application (number, date_of_submission, start_date, end_date, application_scan, comment, applicant_id)";
			sql = sql + String.format(" values ('%s','%s','%s','%s','%s','%s','%s')", application.getNumber(),
					application.getDateOfSubmission(), application.getStartDate(), application.getEndDate(),
					application.getApplicationScan(), application.getComment(), application.getApplicantId());
		}
		performSqlSelect(sql);
	}

	public static void savePersonAbility(Ability ability) {
		String sql = null;
		if (ability.getId() > 0) {
			sql = String.format(
					"UPDATE ability_to_teach SET since = '%s', till = '%s', subject_id = '%s' WHERE id = '%s'",
					ability.getSince(), ability.getTill(), ability.getSubjectId(), ability.getId());

		} else {
			sql = "INSERT INTO ability_to_teach (since, till, subject_id, person_id)";
			sql = sql + String.format(" values ('%s','%s','%s','%s')", ability.getSince(), ability.getTill(),
					ability.getSubjectId(), ability.getPersonId());
		}
		performSqlSelect(sql);
	}

	public static ResultSet performSqlSelect(String sql) {
		Connection conn = null;
		Statement stmt = null;

		try {
			Class.forName("org.mariadb.jdbc.Driver");
			conn = DriverManager.getConnection(CONNECTION_URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
			stmt = conn.createStatement();
			return stmt.executeQuery(sql);

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static Person getPerson(String personId) {
		ResultSet result = PersonService.performSqlSelect("select * from person where id = " + personId);
		try {
			result.next();
			return new Person(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Application[] getPersonApplications(String personId) {
		List<Application> applications = new ArrayList<>();
		ResultSet result = PersonService.performSqlSelect("select * from application where applicant_id = " + personId);
		try {
			while (result.next()) {
				applications.add(new Application(result));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return applications.toArray(new Application[0]);
	}

	public static Application getPersonApplication(String applicationId) {
		ResultSet result = PersonService.performSqlSelect("select * from application where id = " + applicationId);
		try {
			result.next();
			return new Application(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Ability[] getAbilities(String personId) {
		List<Ability> ability = new ArrayList<>();
		ResultSet result = PersonService.performSqlSelect(
				"select ability_to_teach.*, subject.name as subject_name, age_group.name as age_group_name, age_group.min_age, age_group.max_age from ability_to_teach inner join subject on subject.id = ability_to_teach.subject_id inner join age_group on age_group.id = subject.age_group_id where person_id = "
						+ personId);
		try {
			while (result.next()) {
				ability.add(new Ability(result));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ability.toArray(new Ability[0]);
	}

	public static Ability getPersonAbility(String abilityId) {
		ResultSet result = PersonService.performSqlSelect(
				"select ability_to_teach.*, subject.name as subject_name, age_group.name as age_group_name, age_group.min_age, age_group.max_age from ability_to_teach inner join subject on subject.id = ability_to_teach.subject_id inner join age_group on age_group.id = subject.age_group_id where ability_to_teach.id = "
						+ abilityId);
		try {
			result.next();
			return new Ability(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Subject[] getAllSubjects() {
		List<Subject> subject = new ArrayList<>();
		ResultSet result = PersonService.performSqlSelect(
				"select subject.*,age_group.min_age, age_group.max_age, age_group.name as age_group_name from subject inner join age_group on subject.age_group_id = age_group.id");
		try {
			while (result.next()) {
				subject.add(new Subject(result));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return subject.toArray(new Subject[0]);
	}

	public static void saveSubject(Subject subject) {

		String sql = null;
		if (subject.getId() > 0) {
			sql = String.format(
					"UPDATE subject SET code = '%s', name = '%s', comment = '%s', age_group_id = '%s' WHERE id = '%s'",
					subject.getCode(), subject.getName(), subject.getComment(), subject.getAgeGroupId(),
					subject.getId());

		} else {
			sql = "INSERT INTO subject (code, name, comment, age_group_id)";
			sql = sql + String.format(" values ('%s','%s','%s','%s')", subject.getCode(), subject.getName(),
					subject.getComment(), subject.getAgeGroupId());
		}
		performSqlSelect(sql);
	}

	public static Subject getSubject(String subjectId) {
		ResultSet result = PersonService.performSqlSelect("select * from subject where id = " + subjectId);
		try {
			result.next();
			return new Subject(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static AgeGroup[] getAllAgeGroups() {

		List<AgeGroup> ageGroup = new ArrayList<>();
		ResultSet result = PersonService.performSqlSelect("select * from age_group");
		try {
			while (result.next()) {
				ageGroup.add(new AgeGroup(result));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ageGroup.toArray(new AgeGroup[0]);
	}

	public static Person getAuthentication(String email, String pword) {
		
		ResultSet result = PersonService.performSqlSelect("select * from person where mailing_address = '" + email + "' and  password = '" + pword + "'");
		try {
			result.next();
			return new Person(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
		
	
}
