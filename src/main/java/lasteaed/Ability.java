package lasteaed;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Ability {

	private int id;
	private String since = "000-01-01";
	private String till = "000-01-01";
	private String comment;
	private int subjectId;
	private int personId;
	private String subjectName;
	private String ageGroupName;
	private int minAge;
	private int maxAge;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSince() {
		return since;
	}

	public void setSince(String since) {
		this.since = since;
	}

	public String getTill() {
		return till;
	}

	public void setTill(String till) {
		this.till = till;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public Ability() {
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getAgeGroupName() {
		return ageGroupName;
	}

	public void setAgeGroupName(String ageGroupName) {
		this.ageGroupName = ageGroupName;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public Ability(ResultSet resultSetRow) {

		try {
			this.id = resultSetRow.getInt(1);
			this.since = resultSetRow.getString(2);
			this.till = resultSetRow.getString(3);
			this.comment = resultSetRow.getString(4);
			this.subjectId = resultSetRow.getInt("subject_id");
			this.personId = resultSetRow.getInt("person_id");
			this.subjectName = resultSetRow.getString("subject_name");
			this.ageGroupName = resultSetRow.getString("age_group_name");
			this.minAge = resultSetRow.getInt("min_age");
			this.maxAge = resultSetRow.getInt("max_age");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
