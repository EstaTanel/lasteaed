package lasteaed;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Subject {
	private int id;
	private String code;
	private String name;
	private String comment;
	private int ageGroupId;
	private String ageGroupName;
	private int minAge;
	private int maxAge;
	
	
	
	
	
	public String getAgeGroupName() {
		return ageGroupName;
	}

	public void setAgeGroupName(String ageGroupName) {
		this.ageGroupName = ageGroupName;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getAgeGroupId() {
		return ageGroupId;
	}

	public void setAgeGroupId(int ageGroupId) {
		this.ageGroupId = ageGroupId;
	}

	public Subject() {
	}
	
	public Subject(ResultSet resultSetRow) {

		try {
			this.id = resultSetRow.getInt(1);
			this.code = resultSetRow.getString(2);
			this.name = resultSetRow.getString(3);
			this.comment = resultSetRow.getString(4);
			this.ageGroupId = resultSetRow.getInt("age_group_id");
			this.ageGroupName = resultSetRow.getString("age_group_name");
			this.minAge = resultSetRow.getInt("min_age");
			this.maxAge = resultSetRow.getInt("max_age");
			

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
