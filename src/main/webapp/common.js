/**
 * common javascript functions
 */
var currentlyActiveApplicationId = 0;
var currentlyActiveAbilitiesId = 0;
var currentlyActiveSubjectId = 0;

function openApplicationModal(applicationId) {
	currentlyActiveApplicationId = applicationId;
	getApplication(applicationId);
	$('#personApplicationModal').modal('show');
}

function newApplicationModal() {
	currentlyActiveApplicationId = 0;
	$('#number').val('');
	$('#date_of_submission').val('');
	$('#start_date').val('');
	$('#end_date').val('');
	$('#application_comment').val('');
	$('#application_scan').val('');
	$('#personApplicationModal').modal('show');
}

function openAbilitiesModal(abilitiesId) {
	currentlyActiveAbilitiesId = abilitiesId;
	getPersonAbility(abilitiesId);
	$('#personAbilityModal').modal('show');
}

function openNewAbilityModal() {
	currentlyActiveAbilitiesId = 0;
	$('#subject_id').val('');
	$('#since').val('');
	$('#till').val('');
	$('#personAbilityModal').modal('show');
}

function openNewSubjectModal() {
	currentlyActiveSubjectId = 0;
	$('#newSubjectModal').modal('show');

}

function openSubjectModal(subjectId) {
	currentlyActiveSubjectId = subjectId;
	getSubject(subjectId);
	$('#code').val('');
	$('#name').val('');
	$('#comment').val('');
	$('#age_group_id').val('');
	$('#subjectModal').modal('show');

}

function openUserModal() {
	$('#mailing_address').val('');
	$('#password').val('');
	$('#openUserModal').modal('show');

}

function savePersonApplication() {
	$.ajax({
		url : '/lasteaed/rest/savepersonapplication',
		dataType : "json",
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify({
			"id" : currentlyActiveApplicationId,
			"applicantId" : getParameterByName("person_id"),
			"number" : $("#number").val(),
			"dateOfSubmission" : $("#date_of_submission").val() != "" ? $(
					"#date_of_submission").val() : "0001-01-01",
			"startDate" : $("#start_date").val() != "" ? $("#start_date").val()
					: "0001-01-01",
			"endDate" : $("#end_date").val() != "" ? $("#end_date").val()
					: "0001-01-01",
			"applicationScan" : $("#application_scan").val(),
			"comment" : $("#application_comment").val()

		}),
		complete : function(result) {
			if (result.responseText == 'OK') {
				$('#personApplicationModal').modal('hide');
				getPersonApplications();
			}
		}

	});
}

function savePerson() {
	var currentId = 0;
	if (getParameterByName("person_id") > 0) {
		currentId = getParameterByName("person_id");
	}
	$.ajax({
		url : '/lasteaed/rest/person',
		dataType : "json",
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify({

			"id" : currentId,
			"personalCode" : $("#personal_code").val(),
			"nationality" : $("#nationality").val(),
			"firstName" : $("#first_name").val(),
			"lastName" : $("#last_name").val(),
			"sex" : $("#sex").val(),
			"dateOfBirth" : $("#date_of_birth").val() != "" ? $(
					"#date_of_birth").val() : "0001-01-01",
			"postalAddress" : $("#postal_address").val(),
			"mailingAddress" : $("#mailing_address").val(),
			"levelOfEducation" : $("#level_of_education").val(),
			"acquiredSpeciality" : $("#acquired_speciality").val(),
			"comment" : $("#comment").val()
		}),
		complete : function(result) {
			if (result.responseText == 'OK') {
				$('#exampleModal').modal('hide');
				getPersons();

			}
		}

	});
}

function getParameterByName(name, url) {
	if (!url)
		url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex
			.exec(url);
	if (!results)
		return null;
	if (!results[2])
		return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getPersons() {
	$
			.get(
					"/lasteaed/rest/persons",
					function(result) {
						$('#persons_list').empty();

						for (var i = 0; i < result.length; i++) {

							var personBody = 'Isikukood: '
									+ result[i].personalCode + '<br>';
							personBody += 'Kodakondsus: '
									+ result[i].nationality + '<br>';
							personBody += 'Eesnimi: ' + result[i].firstName
									+ '<br>';
							personBody += 'Perenimi: ' + result[i].lastName
									+ '<br>';
							personBody += 'Sugu: ' + result[i].sex + '<br>';
							personBody += 'Sünniaeg: '
									+ result[i].dateOfBirth + '<br>';
							personBody += 'Postiaadress: '
									+ result[i].postalAddress + '<br>';
							personBody += 'Meiliaadress: '
									+ result[i].mailingAddress + '<br>';
							personBody += 'Haridustase: '
									+ result[i].levelOfEducation + '<br>';
							personBody += 'Omandatud eriala: '
									+ result[i].acquiredSpeciality + '<br>';
							personBody += 'Kommentaar: ' + result[i].comment
									+ '<br>';
							personBody += '<a class="btn btn-primary btn-sm" href="person.html?person_id='
									+ result[i].id + '">Vaata lähemalt</a><br>';

							$('#persons_list').append('<div class="card">');
							$('#persons_list')
									.append(
											'<div class="card-header" id="heading'
													+ i
													+ '"><h5 class="mb-0"><button class="btn btn-link" data-toggle="collapse" data-target="#collapse'
													+ i
													+ '" aria-expanded="true" aria-controls="collapse'
													+ i + '">'
													+ result[i].firstName + ' '
													+ result[i].lastName
													+ '</button></h5></div>');
							$('#persons_list')
									.append(
											'<div id="collapse'
													+ i
													+ '" class="collapse " aria-labelledby="heading'
													+ i
													+ '" data-parent="#persons_list"><div class="card-body">'
													+ personBody
													+ '</div></div></div>');
							$('#persons_list').append('</div>');

						}
					});
}

function setupControls() {
	$(".calendar-view").datepicker({
		dateFormat : 'yy-mm-dd',
		showButtonPanel : true,
		changeMonth : true,
		changeYear : true,
		showOtherMonths : true,
		selectOtherMonths : true,
		yearRange : "-50:+50"
	});
}

function getPersonApplications() {
	$
			.get(
					"/lasteaed/rest/getpersonapplications?person_id="
							+ getParameterByName("person_id"),
					function(result) {
						$('#person_applications').empty();

						for (var i = 0; i < result.length; i++) {

							var applicationBody = 'Number: ' + result[i].number
									+ '<br>';
							applicationBody += 'Avalduse kuupäev: '
									+ result[i].dateOfSubmission + '<br>';
							applicationBody += 'Algus: '
									+ result[i].startDate + '<br>';
							applicationBody += 'Lõpp: ' + result[i].endDate
									+ '<br>';
							applicationBody += 'Dokument sisestatud: '
									+ result[i].applicationScan + '<br>';
							applicationBody += 'Kommentaar: ' + result[i].comment
									+ '<br>';
							applicationBody += 'Avaldaja id: '
									+ result[i].applicantId + '<br>';
							applicationBody += '<button type="button" class="btn btn-primary" data-toggle="modal" onClick = "openApplicationModal('
									+ result[i].id
									+ ')" >Muuda avaldust</button>';

							$('#person_applications').append(
									'<div class="card">');
							$('#person_applications')
									.append(
											'<div class="card-header" id="heading'
													+ i
													+ '"><h5 class="mb-0"><button class="btn btn-link" data-toggle="collapse" data-target="#collapse'
													+ i
													+ '" aria-expanded="true" aria-controls="collapse'
													+ i + '">'
													+ result[i].number
													+ '</button></h5></div>');
							$('#person_applications')
									.append(
											'<div id="collapse'
													+ i
													+ '" class="collapse " aria-labelledby="heading'
													+ i
													+ '" data-parent="#persons_list"><div class="card-body">'
													+ applicationBody
													+ '</div></div></div>');
							$('#person_applications').append('</div>');

						}
					});
}

function getApplication(applicationId) {
	$.get(
			"/lasteaed/rest/getpersonapplication?application_id="
					+ applicationId, function(result) {
				$('#number').val(result.number);
				$('#date_of_submission').val(result.dateOfSubmission);
				$('#start_date').val(result.startDate);
				$('#end_date').val(result.endDate);
				$('#application_comment').val(result.comment);
				$('#application_scan').val(result.applicationScan);
				$('#applicant_id').val(result.applicantId);

			});

}

function getPerson() {
	$.get("/lasteaed/rest/getperson?person_id="
			+ getParameterByName("person_id"), function(result) {
		$('#person_details').empty();

		$('#personal_code').val(result.personalCode);
		$('#nationality').val(result.nationality);
		$('#first_name').val(result.firstName);
		$('#last_name').val(result.lastName);
		$('#sex').val(result.sex);
		$('#date_of_birth').val(result.dateOfBirth);
		$('#postal_address').val(result.postalAddress);
		$('#mailing_address').val(result.mailingAddress);
		$('#level_of_education').val(result.levelOfEducation);
		$('#acquired_speciality').val(result.acquiredSpeciality);
		$('#comment').val(result.comment);

	});
}

function getAbilities() {
	$
			.get(
					"/lasteaed/rest/getpersonabilities?person_id="
							+ getParameterByName("person_id"),
					function(result) {

						$('#person_abilities').empty();

						for (var i = 0; i < result.length; i++) {
							
							var tableRow = '<tr><td>'
									+ result[i].subjectName
									+ '</td><td>'
									+ result[i].ageGroupName
									+ '</td><td>'
									+ result[i].minAge
									+ '</td><td>'
									+ result[i].maxAge
									+ '</td><td>'
									+ result[i].since
									+ '</td><td>'
									+ result[i].till
									+ '</td><td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onClick = "openAbilitiesModal('
									+ result[i].id
									+ ')">Muuda andmeid</button></td> </tr>';

							$('#person_abilities').append(tableRow);
						}
					});
}

function getPersonAbility(abilityId) {
	$.get("/lasteaed/rest/getpersonability?ability_id=" + abilityId, function(
			result) {
	
		$('#subject_id').val(result.subjectId);
		$('#since').val(result.since);
		$('#till').val(result.till);

	});

}

function savePersonAbility() {
	$.ajax({
		url : '/lasteaed/rest/savepersonability',
		dataType : "json",
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify({
			"id" : currentlyActiveAbilitiesId,
			"since" : $("#since").val() != "" ? $("#since").val()
					: "0001-01-01",
			"till" : $("#till").val() != "" ? $("#till").val() : "0001-01-01",
			"subjectId" : $("#subject_id").val(),
			"personId" : getParameterByName("person_id")
		
		}),
		complete : function(result) {
			if (result.responseText == 'OK') {
				$('#personAbilityModal').modal('hide');
				getAbilities();
			}
		}

	});
}

function setupSubjectControl() {
	$.get("/lasteaed/rest/getallsubjects", function(result) {
		$('#subject_id').empty();

		for (var i = 0; i < result.length; i++) {
			$('#subject_id').append(
					'<option value="' + result[i].id + '">' + result[i].name
							+ ' (alates ' + result[i].minAge + ' kuni '
							+ result[i].maxAge + ')</option>'); 
		}
	});
}

function saveSubject() {
	$.ajax({
		url : '/lasteaed/rest/savesubject',
		dataType : "json",
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify({
			"id" : currentlyActiveSubjectId,
			"code" : $("#code").val(),
			"name" : $("#name").val(),
			"comment" : $("#comment").val(),
			"ageGroupId" : $("#age_group_id").val()

		}),
		complete : function(result) {
			if (result.responseText == 'OK') {
				$('#subjectModal').modal('hide');
				getAllSubjects();
			}
		}

	});
}

function getAllSubjects() {
	$
			.get(
					"/lasteaed/rest/getallsubjects",
					function(result) {

						$('#allsubjects_list').empty();

						for (var i = 0; i < result.length; i++) {

							var tableRow = '<tr><td>'
									+ result[i].code
									+ '</td><td>'
									+ result[i].name
									+ '</td><td>'
									+ result[i].comment
									+ '</td><td>'
									+ result[i].ageGroupId
									+ '</td><td><button type="button" class="btn btn-primary" onClick = "openSubjectModal('
									+ result[i].id
									+ ')">Muuda õppeainet</button></td> </tr>';

							$('#allsubjects_list').append(tableRow);
						}
					});
}

function setupAgeGroupControl() {
	$.get("/lasteaed/rest/getallagegroups", function(result) {
		$('#age_group_id').empty();

		for (var i = 0; i < result.length; i++) {
			$('#age_group_id').append(
					'<option value="' + result[i].id + '">' + result[i].name
							+ ' (alates ' + result[i].minAge + ' kuni '
							+ result[i].maxAge + ')</option>'); 
		}
	});
}

function getSubject(subjectId) {
	$.get("/lasteaed/rest/getsubject?subject_id=" + subjectId,
			function(result) {
				$('#code').val(result.code);
				$('#name').val(result.name);
				$('#comment').val(result.comment);
				$('#age_group_id').val(result.ageGroupId);

			});

}

function signIn() {
	$.ajax({
		url : '/lasteaed/rest/getauthentication',
		type : 'post',
		data : {
			"mailing_address" : $("#mailing_address").val(),
			"password" : $("#password").val()
		},
		complete : function(result) {
			if (result.responseText == 'ok') {
				// window.location = 'persons.html';
				$('#login_button').hide();
				$('#logout_button').show();
				$('#openUserModal').modal('hide');

			} else {
				alert('Authentication failed!');
			}
		}
	});
}

function logOut() {
	$.ajax({
		url : '/lasteaed/rest/getlogoffperson',
		type : 'get',
		complete : function(result) {
			if (result.responseText == 'OK') {
				$('#logout_button').hide();
				$('#login_button').show();
				alert('Uute kohtumisteni!')
			}
		}
	});
}

function getAuthenticatedPerson() {
	$.get("/lasteaed/rest/getauthenticatedperson", function(result) {
		if (result.id != 0) {
			$('#logout_button').show();
			$('#login_button').hide();
		} 
		else {
			$('#logout_button').hide();
			$('#login_button').show();
		}
	});

}