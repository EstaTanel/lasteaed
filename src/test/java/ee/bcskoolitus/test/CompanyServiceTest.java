package ee.bcskoolitus.test;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;

import ee.bcsvaliitcompany.dao.Company;

public class CompanyServiceTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPerformSqlSelect() throws SQLException {
		ResultSet result = CompanyService.performSqlSelect("select * from company");
		assertTrue(result.next());
		
	}
	@Test
	public void testGetCompanies( ) {
		Company[] companies = CompanyService.getCompanies();
		assertTrue(companies != null);
		assertTrue(companies.length > 0);
		assertTrue(companies[0].getId() > 0);
		
	}

}
